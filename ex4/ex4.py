import matplotlib.pyplot as plt
from numpy import zeros
from numpy.fft import fft, ifft
import numpy as np
from scipy.signal import lfilter
import control as co

# Fonction de dirac pour les n entiers
def dirac(n):
    d = zeros(n)
    d[0] = 1
    return d

a = 0.8
num = np.array([1])
den = np.array([1,-a])
sys = co.tf(num,den)

b,p,w=co.bode(sys)
plt.plot(w,b)
plt.plot(w,p)
plt.show()

a = -0.8
num = np.array([1])
den = np.array([1,-a])
sys = co.tf(num,den)

b,p,w=co.bode(sys)
plt.plot(w,b)
plt.plot(w,p)
plt.show()

# plt.plot(abs(H),label=u"Réponse en fréquence")
# plt.legend()
# plt.xlabel("Time [s]")
# plt.ylabel("Amplitude")
# plt.grid(visible=True)
# plt.show()
