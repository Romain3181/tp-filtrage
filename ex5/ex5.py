import math
import matplotlib.pyplot as plt
from scipy.signal import lfilter
import numpy as np

# Fonction de dirac pour les n entiers
def dirac(n):
    d=np.zeros(n)
    d[0]=1
    return d

# 1)

# Fonction sinus avec les caractéristiques demandées
f0 = 3
def x(t):
    return math.sin(f0 * 2 * math.pi * t)

# Calcul du signal
N = 128
T = 4
dt = T/N
data = []
time = []

for k in range(N):
    time.append(k*dt)
    data.append(x(k*dt))

plt.plot(time, data, label="signal")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Amplitude")
plt.grid(visible=True)
plt.show()

# 2)

a = 0.8
# Utilisation de lfilter
y1 = lfilter([1], [1, -a], data)
plt.plot(time, y1, label="y1")
# Calcul de la réponse impulsionnelle
h = lfilter([1], [1, -a], dirac(N))
# Filtrage en utilisant h
y2 = lfilter(h, [1], data)
plt.plot(time, y2, label="y2")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Amplitude")
plt.grid(visible=True)
plt.show()

# Différence entre les deux méthodes de filtrage
y3 = y1 - y2
plt.plot(time, y3, label="y1 - y2")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Amplitude")
plt.grid(visible=True)
plt.show()