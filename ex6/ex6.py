import math
import matplotlib.pyplot as plt
from scipy.signal import lfilter, firwin
import numpy as np

# Signal sinusoidal
def x(t):
    return 100 * math.sin(26 * math.pi * t)

# 1)

N = 200
T = 0.5
dt = T/N
data = []
data_noise = []
time = []

# Modifier le sigma pour ajouter ou réduire la puissance du bruit
sigma = 4
# Génération du bruit blanc suivant la loi normal de paramètres 0 et sigma²
noise = np.random.normal(0, math.pow(sigma, 2), N)
for k in range(N):
    time.append(k*dt)
    data.append(x(k*dt))
    data_noise.append(x(k*dt) + noise[k])

# Affichage du signal d'origine ainsi que du signal bruité
plt.plot(time, data_noise, label="signal bruité")
plt.plot(time, data, label="signal")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Amplitude")
plt.show()

# 3)

# Débruitage du signal
h = firwin(8, 0.1)
result_signal = lfilter(h, [1], data_noise)

# Affichage du signal débruité
plt.plot(time, result_signal, label="signal débruité")
plt.plot(time, data, label="signal")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Amplitude")
plt.show()