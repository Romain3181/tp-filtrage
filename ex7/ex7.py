from os.path import dirname, join as pjoin
from scipy.io import wavfile
import matplotlib.pyplot as plt
import numpy as np

# Extraction des données du fichier wav
wav_fname = pjoin(dirname(__file__), 'wilhelm.wav')
rate, data = wavfile.read(wav_fname)
length = data.shape[0] / rate
time = np.linspace(0., length, data.shape[0])

# 2)

# Affichage du signal
plt.plot(time, data[:, 0], label="Left channel")
plt.plot(time, data[:, 1], label="Right channel")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Amplitude")
plt.show()

# 3)

# Calcul du module et de la phase
tf = np.fft.fft(data[:, 0])
module = np.abs(tf[0:tf.size//2])
phase = np.angle(tf[0:tf.size//2])
freq = np.fft.fftfreq(tf.size, d=1/rate)
freq2 = freq[:tf.size//2]

# Affichage du module
plt.plot(freq2, 2/tf.size*module, label="module")
plt.legend()
plt.xlim(0, 5000)
plt.xlabel("Time [s]")
plt.ylabel("Amplitude")
plt.show()

# Affichage de l'argument
plt.plot(freq2, 2/tf.size*phase, label="angle")
plt.legend()
plt.xlim(0, 5000)
plt.xlabel("Time [s]")
plt.ylabel("Amplitude")
plt.show()