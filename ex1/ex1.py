import math
import numpy as np
from numpy.fft import fft
import matplotlib.pyplot as plt

# Signal
def x(t):
    return math.sin(100 * math.pi * t) + 0.5 * math.cos(400 * math.pi * t)

# Calcul du signal
N = 1000
T = 1
dt = T/N
data = []
time = []
for k in range(N):
    time.append(k*dt)
    data.append(x(k*dt))

# 1)

plt.plot(time, data)
plt.xlabel('t')
plt.ylabel('y')
plt.show()

# 2)

# Calcul du module et de l'argument
tf = np.fft.fft(data)
module = np.abs(tf)
angle = np.angle(tf)

plt.plot(time, module)
plt.show()

plt.plot(time, angle)
plt.show()