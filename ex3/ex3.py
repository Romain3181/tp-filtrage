import math
from scipy import signal
from scipy.signal import lfilter
from pylab import *

# 1)

# Fonctions Réponse impulsionnelle
def y(a, n):
    yn = 0
    if n > 0 : 
        yn = y(a, n - 1)
    
    return a*yn + dirac(n)

def dirac(n):
    if n == 0 : 
        d = 1
    else :
        d = 0
    return d

def draw_ri(a):
    N = 20
    data = []
    time = []
    for k in range(N):
        if k > 0:
            time.append(k)
            data.append(y(a,k))

    h = lfilter([1], [1,-a], data)
    plt.stem(time, data, label=f'signal (a = {a})')
    plt.plot(time, h, label=f'RI (a = {a})')
    plt.xlabel('n')
    plt.ylabel('h(n)')
    plt.legend()
    plt.show()

a=0.8
draw_ri(a)

# 3)

# Changement de a
# Pour voir les graphiques, il faut fermer le précédent afin que le suivant s'affiche
a=-0.8
draw_ri(a)
a=0.99
draw_ri(a)
a=1.01
draw_ri(a)