import numpy as np
import control as co
import matplotlib.pyplot as plt
from scipy import signal as sig

# H1 

# 1)
# Diagramme de Bode avec l'amplitude et la phase
num=np.array([1,2,4])
den=np.array([1,4,3])
sys=co.tf(num,den)
t,y = co.step_response(sys)
w,mag,ph = co.bode(sys) 
plt.figure()
plt.plot(t,y)
plt.title("Réponse indicielle H1")
plt.show()

# 2)
# Pôles et Zéros
poles = np.roots(den)
zeros = np.roots(num)

# Affichage des pôles et des zéros
plt.scatter(poles.real, poles.imag, label="Poles")
plt.scatter(zeros.real, zeros.imag, label="Zeros")
plt.ylabel("imaginaires")
plt.xlabel("reels")
plt.legend()
plt.axis([-5, 5, -3, 3])
plt.axhline(0, color='black')
plt.axvline(0, color='black')
plt.grid(visible='true')
plt.show()

# H2
# 2)
num = np.array([2])
den = np.array([1,3,3,1])
sys=co.tf(num,den)

t,y = co.step_response(sys)
w,mag,ph = co.bode(sys) 
plt.figure()
plt.plot(t,y)
plt.title("Réponse indicielle H2")
plt.show()

#Pôles et Zeros
poles = np.roots(den)
zeros = np.roots(num)

# Affichage des pôles et des zéros
plt.scatter(poles.real, poles.imag, label="Poles (10e-6)")
plt.scatter(zeros.real, zeros.imag, label="Zeros")
plt.ylabel("imaginaires")
plt.xlabel("reels")
plt.legend()
plt.axhline(0, color='black')
plt.axvline(0, color='black')
plt.grid(visible='true')
plt.show()


# 3) 

num,den = sig.zpk2tf([1], [2,0.5],3)
sys=co.tf(num,den)

t,y = co.step_response(sys)
w,mag,ph = co.bode(sys) 
plt.plot(t,y)
plt.title("Réponse indicielle H3")
plt.show()


